# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # online documentation - https://docs.vagrantup.com.

  vm_boxname = 'FST-CAT-CS-MOD002619'
  vm_boxversion = '2017-1.0'
  vm_hostname = 'rubydev-vm'
  vm_description = 'This virtualmachine has been provided by Anglia Ruskin University in support of Module: MOD002619 - Web Programming'
  vm_iconfile = "./resources/VBoxIcon.png"
  # Configure VBox bios screen splash image.
  imgpath = "resources/VBoxLogo.bmp"

  # Select the 'box' on which to base this.
  # Preferably one of the vendor 'official' ones, or an in-house build
  # See:
  config.vm.box = "ubuntu/trusty64"
    # NOTE: due to 'faff inducing' bug #1569237 we stick with ubuntu server v14.04 (Trusty)
    # NOTE: see https://bugs.launchpad.net/cloud-images/+bug/1569237

  # Automatic box update checking.
  # If 'false' boxes will only be checked for updates when
  # `vagrant box outdated` is run manually. This is not recommended!
  config.vm.box_check_update = true

  # Create port forward for 3000 to allow serving project with mongrel

  config.vm.network "forwarded_port", guest:3000, guest_ip: "192.168.23.23", host: 3000,  host_ip: "127.0.0.1"

  # ssh is preconfigured by vagrant on port 2222
  #config.vm.network "forwarded_port", guest: 22, host: 10022,  host_ip: 127.0.0.1

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.23.23"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  #config.vm.synced_folder "~/ruby-dev", "/home/dev_user/"


  # Generate full path to image for bios boot screen
  basedir = File.expand_path(File.dirname(__FILE__))
  biosimage = basedir + "/" + imgpath

  # virtualbox docs: <https://www.virtualbox.org/manual/ch08.html#vboxmanage-modifyvm>

  vm_boxnamefull = vm_boxname + "-" + vm_boxversion

  config.vm.provider "virtualbox" do |vb|
    # Set the VM name
    vb.name = vm_boxnamefull
    # Display the VirtualBox GUI when booting the machine?
    vb.gui = false
    # Customize the amount of memory on the VM:
    vb.memory = "2048"
    # Set number of CPUs
    vb.cpus = 1


    vb.customize ["modifyvm", :id,
      "--bioslogofadein",      "on",
      "--bioslogofadeout",     "on",
      "--bioslogodisplaytime", 5000,
      "--bioslogoimagepath",   biosimage,
      "--biosbootmenu", "disabled",
      "--natdnsproxy1","on",
      "--natdnshostresolver1", "on",
      "--description",vm_description,
      "--iconfile",vm_iconfile]

  end

  # Set system hostname

  config.vm.hostname = vm_hostname

  # Set provisioning script for heavy lifting

  config.vm.provision :shell, :path => "./resources/script.sh"

  config.vm.post_up_message = "\033[01;37mThe virtual machine is now ready to use. \nYou can access it from the Virtualbox application...\n or...\n from the commandline:\nVBoxManage startvm " + vm_boxnamefull

end
