#Ruby on Rails VM

##ubuntu-server edition

### Prerequisites

Host machine:

+ Virtualbox
    +    <https://www.virtualbox.org/wiki/Downloads>
+ vagrant
    + (macOS Homebrew)<br />`brew install Caskroom/casks/vagrant`
    + (all) <https://www.vagrantup.com/downloads.html>
+ git
    + (macOS) XCode Commandline Tools<br /> `xcode-select --install`
    + (macOS Homebrew) <br />`brew install git`
    + (all) <https://git-scm.com/downloads>

---

### What is in this repo ?

+ `Vagrantfile` - [Vagrant](https://www.vagrantup.com/) template file
+ `setup.sh` - Updates OS, installs software, creates users, customises UX, makes the tea (if only)
+ `resources/` - files required to configure the VM.

---

### What does it do ?

Following the instructions below will:

+ Download 'base' box of **Ubuntu Server LTS 14.04** (this only happens once, is cached after this to save bandwidth)
+ Build VM according to  Vagrant config file: `./Vagrantfile`
+ Configure VM according to `./resources/setup.sh`

---

### Building the VM

1. Ensure that you meet all of the prerequisites - git, virtualbox and vagrant
2. Clone this repo `git clone` or `git pull` if this has already been done.
3. In the resultant `rubydev-vm` dir, build the vm:
`cd rubydev-vm && vagrant up`
4. The VM will be intialised, packages installed, users created etc etc
5. Once the script is finished, the VM will be ready to use.
6. In virtualbox, start the VM
7. The onscreen instructions provide information as to how to access the various services running on the VM

Connect to ubuntu-server console `ssh -p 2222 dev_user@localhost`

##### Rebuilding the VM

As development continues it will be necessary to **destroy** the created VM and rebuild it.

To destroy : `vagrant destroy` 
Then `git pull` to get the latest version of the repo
Then `vagrant up` to rebuild the VM

---

### Configured services

The VM is intended to provide a consistent, ready-to-go development environment.

The following services are pre-configured

Configured users:

+ vagrant - this is the user account used for the provisioning of the VM
+ dev_user - this is the account intended for students to use. The username can be changed to whatever makes the most sense, maybe even just **student** ?

+ Ruby 2.3.3 via [rvm]() (we can use rbenv instead if preferred)
+ Rails 4.2.6
+ nginx + passenger
    + nginx listens on:
        + 192.168.23.23:80 - Quickstart guide
        + 192.168.23.23:2000 - devdocs
        + 192.168.23.23:3000 - rails server (or custom 502 error page if not running)

---

#### Accessing the VM

First, go to <http://192.168.23.23> - This would be where initial instructions (a 'Getting Started' guide) could go

Then, see the links across the top of the page:

<http://192.168.23.23:3000> - This location is a reverse proxy port 3000 on the VM (see the note in the Development section below) served by [nginx](http://nginx.com). 

Until the rails server is running on the VM, this displays the custom 502.html file (see `resources/502.html`. This is actually the error page from nginx for **502 - Bad Gateway** made more friendly with a bit of css and js.

As soon as `rails s` is running, the page is replaced by the output of the rails server. Neat huh!?

Also see: <http://192.168.23.23:2000> where local copies of documentation for ruby, rails, html, css etc are provided. These are served using the <http://devdocs.io> application, which is served using passenger in the VM. 

Any of the document sets available at <http://devdocs.io> can be included in the VM. 


---
# Development notes:


#### Rails server only works for localhost without 'binding'

Since version 4.2 the rails server (puma) defaults to [accepting connections from localhost](http://guides.rubyonrails.org/4_2_release_notes.html)(127.0.0.1) **_only_**. This is a sensible choice in that it prevents developers from inadvertantly exposing their web application to the world. It is problematic in that it means a simple port NAT port forward in Virtualbox will no longer allow easy connection to a rails server running inside a virtual machine.

#####The solution:

The VM therefore contains an instance of [nginx]() configured to act as a reverse proxy for all traffic on port 3000. In short, this allows connection to the rails server inside the vm without having to change the default behaviour, either by manually binding the rails server to a different ip `rails server -b 0.0.0.0`


#### shared folders

Relative paths - <https://www.virtualbox.org/ticket/15305>

#### Devdocs

Create a local documentation set, serve this with nginx and passenger at http://192.168.23.23/docs

```
git clone https://github.com/Thibaut/devdocs.git && cd devdocs
gem install bundler
bundle install
thor docs:download --default
rackup
```

Also see <https://github.com/Thibaut/devdocs/issues/314>

#### Passenger

from <https://www.phusionpassenger.com/library/install/nginx/install/oss/trusty/>

```
# Install our PGP key and add HTTPS support for APT
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo apt-get install -y apt-transport-https ca-certificates

# Add our APT repository
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'
sudo apt-get update

# Install Passenger + Nginx
sudo apt-get install -y nginx-extras passenger

```
uncomment/add to `/etc/nginx.conf`

```

http {
    include /etc/nginx/passenger.conf;
    ...
}
```

Restart nginx
`sudo service nginx restart`

Tests (do not include in vagrant)
`sudo /usr/bin/passenger-config validate-install`
`sudo /usr/sbin/passenger-memory-stats`

DEV nginx conf

```
user www-data;
worker_processes  1;
events {
    worker_connections  1024;
}
http {
    include /etc/nginx/passenger.conf;
    include       mime.types;
    default_type  application/octet-stream;
    send_timeout 1800;
    sendfile        on;
    keepalive_timeout  6500;
    server {
        listen       192.168.23.23:3000;
        server_name  $host;
        location / {
          proxy_pass          http://localhost:3000;
          proxy_set_header    Host             $host;
          proxy_set_header    X-Real-IP        $remote_addr;
          proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
          proxy_set_header    X-Client-Verify  SUCCESS;
          proxy_set_header    X-Client-DN      $ssl_client_s_dn;
          proxy_set_header    X-SSL-Subject    $ssl_client_s_dn;
          proxy_set_header    X-SSL-Issuer     $ssl_client_i_dn;
          proxy_read_timeout 1800;
          proxy_connect_timeout 1800;
        }
        error_page 502 /502.html;
        location = /502.html {
          root /var/www/aru;
        }
    }
  server {
      listen 192.168.23.23:80;
      server_name $host;
      location / {
        root /var/www/aru-help;
      }
  }
  server {
      listen 192.168.23.23:2000;
        server_name $host;
      location / {
           root /var/www/devdocs/public/;
           passenger_enabled on;
           passenger_ruby /usr/local/rvm/gems/ruby-2.3.3/wrappers/ruby;
           passenger_intercept_errors off;
           passenger_friendly_error_pages on;
           rails_env development;

      }
      error_page 502 /502.html;
        location = /502.html {
        root /var/www/aru-help;
      }

  } # server
} #http

```


#### Installing software

Software installation is managed, wherever possible, using apt-get.

Software for installation should be included (space separated) in the **ADDITIONAL\_PKGS** var in `resources/setup.sh`. Check `apt-cache search <pkg> ` to determine the correct thing to put here.

#### Installing ruby on rails

Use rvm.





#### Reverse proxy

nginx acts as a reverse proxy, connections to the VM on port 3000 are forwarded to the running rails server on localhost:3000. If the rails server is not running, nginx is configured to display an error page linking to the quickstart guide.

#### UX Tweaks

+ [motd-witty](https://codegists.com/snippet/ruby/motd-witty_violetbaddley_ruby)

The motd can be set in a time based fashion

Files are located in the VM at `/etc/update-motd.d/30-motd-witty` and `/etc/motd-witty.json`

TODO: Moving forwards it might be nice to have these auto-update from a git repo somewhere, this could be scheduled as a cron job in the vm


#### ENV Vars

It would be wise to populate some global environment vars to allow easy ID of the VM, it's build no. , the module it was created for etc.

At the moment the module code is stored in /etc/environment as ARU\_MODULE\_CODE and is referenced currently in `/etc/update-motd.d/20-aru-help`

e.g.

```
$ echo $ARU_MODULE_CODE
MOD002619
```

if it isn't picked up, the enviroment file can be sourced in bash scripts : `source /etc/environment`
