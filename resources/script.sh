#!/usr/bin/env bash
#
# Setup script for ARU-FST-CAT-RubyRails3.3.0
# based on <http://dgreen.github.io/blog/2013/06/06/using-bitnamis-rubystack-on-virtualbox-linux-with-vagrant/>

##10############################################################################
# CONFIG

# The user account to create for student use
DEV_USER='dev_user'
# Password for above, leave blank for same as username
DEV_USER_PASSWORD=''

# Module info, this is used to link to the VLE
ARU_MODULE_CODE='MOD002619'
ARU_MODULE_TITLE='Web Programming'

# The VM shared dir from which this script will run
# used for absolute paths
BASE_DIR='/vagrant'
RESOURCE_DIR='resources'

# Password for root and vagrant users
ROOT_PASSWORD='toast.business'
VAGRANT_PASSWORD='toast.business'

# Ruby versions

RUBY_VERSION='2.3.3'
RAILS_VERSION='4.2.6'


# Additional apps to add to the VM
ADDITIONAL_PKGS=' git-core
                  curl
                  zlib1g-dev
                  build-essential
                  libssl-dev
                  libreadline-dev
                  libyaml-dev
                  libsqlite3-dev
                  sqlite3
                  libxml2-dev
                  libxslt1-dev
                  libcurl4-openssl-dev
                  python-software-properties
                  libffi-dev
                  libgdbm-dev
                  libncurses5-dev
                  automake
                  libtool bison
                  ruby-build
                  nodejs
                  nginx
                  nginx-extras
                  passenger
                '

# Remove unnescary apps
REMOVE_PKGS='apache2* mysql* postgres* juju juju-core'

# Additional gems to add to the default image
ADDITIONAL_GEMS="bundler rails:$RAILS_VERSION"

ARU_MODULE=$ARU_MODULE_CODE " - " $ARU_MODULE_TITLE
RESOURCE_PATH=$BASE_DIR"/"$RESOURCE_DIR

# ANSI COLOR ESCAPES for /etc/issue
# TODO: source this from the file in resources?

RESTORE='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'


# Bail on error
set -e

# Print commands and output to ST DOUT
set -x

echo "Shell is $SHELL"

################################################################################
# UX CUSTOMISATIONS

# Configure timezone
locale-gen en_GB.UTF-8

# Customise motd
# chmod -x /etc/update-motd.d/
cp $BASE_DIR/resources/20-aru-help /etc/update-motd.d/20-aru-help
chmod +x /etc/update-motd.d/20-aru-help

## Include the motd-witty
## TODO: clone the gist instead of maintaing a local copy?
#cp $BASE_DIR/resources/30-motd-witty /etc/update-motd.d/30-motd-witty
#cp $BASE_DIR/resources/motd-witty.json /etc
#chmod +x /etc/update-motd.d/30-motd-witty

# disable ubuntu motd elements
chmod -x /etc/update-motd.d/50-landscape-sysinfo
chmod -x /etc/update-motd.d/51-cloudguest
chmod -x /etc/update-motd.d/90-updates-available
chmod -x /etc/update-motd.d/91-release-upgrade

# Configure login banner in /etc/issue
echo "Configure login banner"

echo -e "---------------------------------------
${WHITE}Anglia Ruskin University - Dept. of Computing & Technology
${CYAN}FST-CAT-CS-MOD002619 Virtual Machine${RESTORE}
${CYAN}$ARU_MODULE
${RED}FOR DEVELOPMENT USE ONLY
${WHITE}Support:${GREEN}COM121
${WHITE}Support:${GREEN}01223 698 599
${WHITE}Support:${GREEN}studios@anglia.ac.uk
${WHITE}---------------------------------------
${WHITE}To login here -
${YELLOW}username:${RED}$DEV_USER
${YELLOW}password:${RED}$DEV_USER
${WHITE}---------------------------------------
${WHITE}To connect to the ruby web server (when running)
${YELLOW}Browse to: http://localhost:3000 or http://192.168.23.23:3000
${WHITE}---------------------------------------
${WHITE}To connect to the VM via ssh (Mac/Linux)
${YELLOW}ssh $DEV_USER@192.168.23.23
${WHITE}---------------------------------------
${WHITE}To connect to the VM using putty (windows)
${YELLOW}host: ${RED}192.168.23.23
${YELLOW}login as:${RED}$DEV_USER
${YELLOW}port: ${RED}22
${RESTORE}" > /etc/issue


################################################################################
# USER MANAGEMENT

if [ ! -d /home/$DEV_USER ] ; then
  # Add developer user account
  adduser $DEV_USER
  # Set password same as username (it's a VM, it'll be fine!)
  echo ""$DEV_USER":"$DEV_USER"" | chpasswd
#  echo "export GEM_HOME=~/.gem/ruby/$RUBY_VERSION" >> /home/$DEV_USER/.bashrc
#  echo "export PATH=$PATH:~/.gem/ruby/$RUBY_VERSION/bin" >> /home/$DEV_USER/.bashrc
fi

# Change the password for vagrant and root user.
# This will ultimately stop fiddling and breaking
echo ""root":"$ROOT_PASSWORD"" | chpasswd
echo ""vagrant":"$VAGRANT_PASSWORD"" | chpasswd

# add $DEV_USER to vboxsf group to allow shared folders to work betterer ;-)

usermod -G vboxsf -a dev_user

################################################################################
# PACKAGE MANAGEMENT

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo apt-get install -y apt-transport-https ca-certificates

# Add our APT repository
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'

if [ -n "$ADDITIONAL_PKGS" ] ; then
  # Install additional packages
  echo "Installing additional packages"
  apt-get update
  apt-get install -y $ADDITIONAL_PKGS
fi

if [ -n "$REMOVE_PKGS" ] ; then
  # Install additional packages
  echo "Removing packages"
  apt-get remove -y --purge $REMOVE_PKGS
fi

# Get rid of any other unused packages
apt-get autoremove -y

################################################################################
# RUBY

# Install rvm
# rvm advise against doing this as root
# advice is to use sudo

curl -#LO https://rvm.io/mpapis.asc
gpg --import mpapis.asc
curl -sSL https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
rvm requirements
sudo -u vagrant sudo /usr/local/rvm/bin/rvm install $RUBY_VERSION

rvm use $RUBY_VERSION --default

# Add $DEV_USER to the rvm group
sudo usermod -a -G rvm $DEV_USER


# As $DEV_USER express preference towards ruby version
sudo -u $DEV_USER /usr/local/rvm/bin/rvm use $RUBY_VERSION --default


##gpg --keyserver hkp://pgp.mit.edu --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
#command curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
#curl -sSL https://get.rvm.io | bash -s stable
#source ~/.rvm/scripts/rvm
#rvm install 2.3.1
#rvm use 2.3.1 --default
#ruby -v

# install required gems
if [ -n "$ADDITIONAL_GEMS" ] ; then
  echo "Installing additional gems"
  gem install  $ADDITIONAL_GEMS \
  && echo "Success"
fi

################################################################################
# ENVIRONMENT
#
# n.b. This MUST happen after package installation is done
# as this runs inside the vm and relies on `git` executable
cd /vagrant/
ARU_BUILD_VER=`git describe --tags`
echo ARU_MODULE_CODE="$ARU_MODULE_CODE" >> /etc/environment
echo ARU_BUILD_VER="$ARU_BUILD_VER" >> /etc/environment

# Add ANSI colour escapes to /etc/environment

cat $RESOURCE_PATH/bash_colours >> /etc/environment

################################################################################
# NGINX
#
# Configure nginx to:
# + act as a reverse proxy for mongrel on 127.0.0.1:3000
# + serve up the help files for the virtual machine
# + serv devdocs as a RoR app using passenger

cp $BASE_DIR/resources/nginx.conf /etc/nginx/
mkdir -p /var/www/{aru,aru-help}
cp $BASE_DIR/resources/502.html /var/www/aru
cp -R $BASE_DIR/resources/aru-help/* /var/www/aru-help
chown -R www-data /var/www

################################################################################
# DEVDOCS

# cd /var/www
# git clone https://github.com/Thibaut/devdocs.git && cd devdocs
# #gem install bundler
# #n.b. specifying the path here is a 'remembered setting'
# # any calls to bundle install by root after this will need to specify
# # `bundle install --system`  to return to default behaviour
# bundle install --path ./gems/
# thor docs:download --default
# thor docs:download rails@4.2 ruby@2.4
# chown -R www-data ../devdocs
# chmod -R 777 ../devdocs


################################################################################
# Shared folder mount point setting
# This ensures that shared folders are mounted by default in the DEV_USER /home

VBoxControl guestproperty set /VirtualBox/GuestAdd/SharedFolders/MountDir /home/$DEV_USER
VBoxControl guestproperty get /VirtualBox/GuestAdd/SharedFolders/MountDir

################################################################################
# Shutdown the machine
shutdown -h now
